/**
 * 
 */
package cards;

/**
 * @author ghh
 */
/**
 * An object of type Card represents a playing card from a standard Poker deck, including Jokers. The card has a suit,
 * which can be spades, hearts, diamonds, clubs, or joker. A spade, heart, diamond, or club has one of the 13 values:
 * ace, 2, 3, 4, 5, 6, 7, 8, 9, 10, jack, queen, or king. Note that "ace" is considered to be the smallest value. A
 * joker can also have an associated value; this value can be anything and can be used to keep track of several
 * different jokers.
 */
public class Card {
    /**
     * This card's suit, one of the constants SPADES, HEARTS, DIAMONDS, CLUBS, or JOKER. The suit cannot be changed
     * after the card is constructed.
     */
    public enum Suit {
        SPADES("Spades"),
        HEARTS("Hearts"),
        DIAMONDS("Diamonds"),
        CLUBS("Clubs");
        private final String type;

        public String getType() {
            return type;
        }

        private Suit(String theType) {
            this.type = theType;
        }
    };

    private final Suit suit;

    /**
     * The card's value. For a normal card, this is one of the values 1 through 13, with 1 representing ACE.
     */
    private final Value value;

    public enum Value {
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        TEN(10),
        JACK(11),
        QUEEN(12),
        KING(13),
        ACE(14);
        private final int number;

        public int getNumber() {
            return number;
        }

        private Value(int number) {
            this.number = number;
        }
    };

    /**
     * Creates a card with a specified suit and value.
     * 
     * @param theValue the value of the new card. For a regular card (non-joker), the value must be in the range 1
     *            through 13, with 1 representing an Ace. You can use the constants Card.ACE, Card.JACK, Card.QUEEN, and
     *            Card.KING. For a Joker, the value can be anything.
     * @param theSuit the suit of the new card. This must be one of the values Card.SPADES, Card.HEARTS, Card.DIAMONDS,
     *            Card.CLUBS, or Card.JOKER.
     * @throws IllegalArgumentException if the parameter values are not in the permissible ranges
     */
    public Card(Value theValue, Suit theSuit) {
        value = theValue;
        suit = theSuit;
    }

    /**
     * Returns the suit of this card.
     * 
     * @returns the suit, which is one of the constants Card.SPADES, Card.HEARTS, Card.DIAMONDS, Card.CLUBS
     */
    public String getSuit() {
        return suit.toString();
    }

    /**
     * Returns the value of this card.
     * 
     * @return the value, which is one of the numbers 1 through 13, inclusive for a regular card, and which can be any
     *         value for a Joker.
     */
    public int getValue() {
        return value.getNumber();
    }

    /**
     * Returns a string representation of this card, including both its suit and its value (except that for a Joker with
     * value 1, the return value is just "Joker"). Sample return values are: "Queen of Hearts", "10 of Diamonds",
     * "Ace of Spades", "Joker", "Joker #2"
     */
    public String toString() {
        return getValue() + " of " + getSuit();
    }

} // end class Card