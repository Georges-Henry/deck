/**
 * 
 */
package cards;

/**
 * @author ghh
 */
public class BattleGame {

    private static final int NVALS = 13;
    private Deck cards;
    private Hand player1;
    private Hand player2;

    BattleGame() {
        player1 = new Hand();
        player2 = new Hand();
        cards = new Deck();
        cards.shuffle();
        deal();
    }

    private void deal() {
        boolean flip = true;
        while (!(cards.cardsLeft() == 0)) {
            if (flip)
                player1.addCard(cards.dealCard());
            else
                player2.addCard(cards.dealCard());
            flip = !flip;
        }
    }

    void print() {
        System.out.println("Player 1: " + player1);
        System.out.println("Player 2: " + player2);
    }

    boolean isOver(){
        return player1.hasNoCards() || player2.hasNoCards();
    }

    int winner() {
        if (player1.hasNoCards() && player2.hasNoCards()) {
            return 0;
        }
        if (player1.hasNoCards()) {
            return 2;
        }
        if (player2.hasNoCards()) {
            return 1;
        }
        return 0;
    }


    void oneRound() {
        Hand trick = new Hand();
        if (isOver())
            return;
        Card card1 = player1.play();
        Card card2 = player2.play();
        trick.addCard(card1);
        trick.addCard(card2);
        while (card1.getValue() == card2.getValue()) {
            if (isOver())
                return;
            trick.addCard(player1.play());
            trick.addCard(player2.play());
            if (isOver())
                return;
            card1 = player1.play();
            card2 = player2.play();
            trick.addCard(card1);
            trick.addCard(card2);
        }
        if (card1.getValue() > card2.getValue())
            player1.takeAll(trick);
        else
            player2.takeAll(trick);
    }

    int game(int n) {
        int i = 0;
        while (i < n) {
            oneRound();
            if (isOver())
                return winner();
            i++;
        }
        if (player1.getCardCount() > player2.getCardCount())
            return 1;
        if (player2.getCardCount() > player1.getCardCount())
            return 2;
        return 0;
    }
    public static void main(String[] args) {
        BattleGame bg = new BattleGame();
        System.out.println(bg.game(5));
    }
}
