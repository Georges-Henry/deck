/**
 * 
 */
package cards;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cards.Card.Suit;
import cards.Card.Value;

/**
 * @author ghh
 */

/**
 * An object of type Deck represents a deck of playing cards. The deck is a regular poker deck that contains 52 regular
 * cards and that can also optionally include two Jokers.
 */
public class Deck {

    /**
     * An array of 52 or 54 cards.
     */
    private List<Card> deck;

    /**
     * Keeps track of the number of cards that have been dealt from the deck so far.
     */
    private int cardsUsed;

    /**
     * Constructs a poker deck of playing cards,
     */
    public Deck() {
        deck = new ArrayList<Card>(52);
        int i = 0;
        for (Suit suit : Suit.values()) {
            for (Value value : Value.values()) {
                i++;
                deck.add(new Card(value, suit));
                // System.out.println(value+" "+suit+"==>"+deck.size());
            }
        }
        cardsUsed = 0;
    }

    /**
     * Put all the used cards back into the deck (if any), and shuffle the deck into a random order.
     */
    public void shuffle() {
        Collections.shuffle(deck);
        cardsUsed = 0;
    }

    /**
     * As cards are dealt from the deck, the number of cards left decreases. This function returns the number of cards
     * that are still left in the deck. The return value would be 52 or 54 (depending on whether the deck includes
     * Jokers) when the deck is first created or after the deck has been shuffled. It decreases by 1 each time the
     * dealCard() method is called.
     */
    public int cardsLeft() {
        if (cardsUsed == 52) {
            throw new IllegalStateException("No cards are left in the deck.");
        }
        return deck.size() - cardsUsed - 1;
    }

    /**
     * Removes the next card from the deck and return it. It is illegal to call this method if there are no more cards
     * in the deck. You can check the number of cards remaining by calling the cardsLeft() function.
     * 
     * @return the card which is removed from the deck.
     * @throws IllegalStateException if there are no cards left in the deck
     */
    public Card dealCard() {
        if (cardsUsed+1 == deck.size())
            throw new IllegalStateException("No cards are left in the deck.");
        cardsUsed++;
        return deck.get(cardsUsed);
        // Programming note: Cards are not literally removed from the array
        // that represents the deck. We just keep track of how many cards
        // have been used.
    }

    /**
     * Test whether the deck contains Jokers.
     * 
     * @return true, if this is a 54-card deck containing two jokers, or false if this is a 52 card deck that contains
     *         no jokers.
     */
    public boolean hasJokers() {
        return (deck.size() == 54);
    }

} // end class Deck