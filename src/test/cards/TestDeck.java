/**
 * 
 */
package cards;

//import org.assertj.core.api.Assertions.assertThat;

import org.junit.Before;
import org.junit.Test;

import junit.framework.Assert;

/**
 * @author ghh
 *
 */
public class TestDeck {
    private Deck deck;
    
    @Before
    public void Setup(){
        deck = new Deck();
    }
    
    @Test
    public void shouldDrawCard() {
        Card card = deck.dealCard();
        Assert.assertNotNull(card);
    }
    
    
    @Test(expected=IllegalStateException.class)
    public void testDeck() {
        //System.out.println("++++++"+deck.cardsLeft());
        for (int i = 0; i < 54; i++) {
            deck.dealCard();
            //System.out.println(deck.cardsLeft());
        }
    }
}
